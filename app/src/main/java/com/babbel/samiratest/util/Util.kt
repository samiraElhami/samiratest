package com.babbel.samiratest.util

import android.content.res.Resources

/**
 * generate random number between 0 & upper bound
 * @property start the lower bound value
 * @property end the upper bound value
 * @return random number from the list
 */
fun random_num(start: Int, end: Int): Int {
    return (start..end).random()
}

/**
 * get the screen height
 * @return screen height
 */
fun getScreenHeight(): Float {
    return Resources.getSystem().getDisplayMetrics().heightPixels.toFloat()
}