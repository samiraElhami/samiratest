package com.babbel.samiratest.util

import java.util.*

/**
 * generate random number in specific range
 */
fun IntRange.random() =
    Random().nextInt((endInclusive + 1) - start) + start