package com.babbel.samiratest.util

/**
 * check word status
 * @property status the status of the word meaning
 */
enum class Status(val status: Boolean) {
    RIGHT(true),
    WRONG(false)
}