package com.babbel.samiratest.di

import com.babbel.samiratest.ui.wordActivity.WordActivity
import com.babbel.samiratest.ui.wordActivity.di.WordActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = arrayOf(WordActivityModule::class))
    abstract fun bindWordActivity(): WordActivity
}