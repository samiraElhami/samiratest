package com.babbel.samiratest.di

import android.app.Application
import android.arch.persistence.room.Room
import com.babbel.samiratest.persistence.WordDao
import com.babbel.samiratest.persistence.WordDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideDatabase(app: Application): WordDatabase {
        return Room.databaseBuilder(app.applicationContext,
            WordDatabase::class.java, "word.db")
            .build()
    }

    @Singleton
    @Provides
    fun provideWordDao(db: WordDatabase): WordDao {
        return db.wordDao()
    }
}