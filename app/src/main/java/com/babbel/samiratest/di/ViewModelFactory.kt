package com.babbel.samiratest.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import com.babbel.samiratest.persistence.WordDatabase
import com.babbel.samiratest.ui.wordActivity.WordActivityViewModel

class ViewModelFactory(private val activity: AppCompatActivity): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WordActivityViewModel::class.java)) {
            val db =  Room.databaseBuilder(activity.applicationContext,
                WordDatabase::class.java, "word.db")
                .build()
            return WordActivityViewModel(db.wordDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}