package com.babbel.samiratest.ui.wordActivity

import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModel
import android.content.Context
import android.databinding.ObservableField
import android.util.Log
import android.view.View
import com.babbel.samiratest.R
import com.babbel.samiratest.persistence.Word
import com.babbel.samiratest.persistence.WordDao
import com.babbel.samiratest.util.Status
import com.babbel.samiratest.util.random_num
import com.beust.klaxon.JsonReader
import com.beust.klaxon.Klaxon
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.InputStream
import java.io.StringReader
import java.util.*

class WordActivityViewModel(private val wordDao: WordDao) : ViewModel() {

    val eng_word = ObservableField<String>()
    val spn_word = ObservableField<String>()
    var status_txt = ObservableField<String>()
    var wrong_words = ObservableField<String>()
    var right_words = ObservableField<String>()
    var status_layout_visible = ObservableField<Int>(View.GONE)
    var eng_word_visible = ObservableField<Int>(View.VISIBLE)
    var spn_word_visible = ObservableField<Int>(View.VISIBLE)
    var right_btn_visible = ObservableField<Int>(View.VISIBLE)
    var wrong_btn_visible = ObservableField<Int>(View.VISIBLE)

    lateinit var word_list: List<Word>
    var status: Boolean? = null
    var right_val = 0
    var wrong_val = 0

    companion object {
        private val TAG = WordActivity::class.java.simpleName
    }


    init {
        right_words.set(right_val.toString())
        wrong_words.set(wrong_val.toString())
    }

    /**
     * choose a random word
     */

    fun choose_word() {
        val length = word_list.size - 1
        val current_word_row = random_num(0, length)
        val word = word_list.get(current_word_row)
        eng_word.set(word.text_eng)
        choose_answer(current_word_row)
    }

    /**
     * whether to show right/wrong answer
     * @property current_word_row the row of word
     */
    fun choose_answer(current_word_row: Int) {
        val correct_meaning_val = random_num(0, 1)
        //set wrong spanish meaning
        if (correct_meaning_val == 0) {
            do {
                val length = word_list.size - 1
                val wrong_word_row = random_num(0, length)
                val word = word_list.get(wrong_word_row)
                spn_word.set(word.text_spa)
                status = Status.WRONG.status

            } while (wrong_word_row == current_word_row)

        } //set right spanish meaning
        else {
            val word = word_list.get(current_word_row)
            spn_word.set(word.text_spa)
            status = Status.RIGHT.status
        }
    }


    /**
     * click right button fun
     * set the right/wrong value
     */

    fun right_answer(context: Context) {
        hide_main_view()
        if (status == Status.RIGHT.status) {
            status_txt.set(context.getString(R.string.right_answer_txt))
            right_val++
            right_words.set(right_val.toString())
            wrong_words.set(wrong_val.toString())
        } else {
            status_txt.set(context.getString(R.string.wrong_answer_txt))
            wrong_val++
            right_words.set(right_val.toString())
            wrong_words.set(wrong_val.toString())
        }
    }

    /**
     * click wrong button fun
     * set the right/wrong value
     */

    fun wrong_answer(context: Context) {
        hide_main_view()
        if (status == Status.WRONG.status) {
            status_txt.set(context.getString(R.string.right_answer_txt))
            right_val++
            right_words.set(right_val.toString())
            wrong_words.set(wrong_val.toString())
        } else {
            status_txt.set(context.getString(R.string.wrong_answer_txt))
            wrong_val++
            right_words.set(right_val.toString())
            wrong_words.set(wrong_val.toString())
        }
    }

    /**
     * none of the btns clicked
     * set the wrong value
     */

    fun no_answer(context: Context) {
        hide_main_view()
        status_txt.set(context.getString(R.string.no_answer_txt))
        wrong_val++
        right_words.set(right_val.toString())
        wrong_words.set(wrong_val.toString())
    }

    /**
     * show status view
     * hide other views
     */

    fun show_main_view() {
        status_layout_visible.set(View.GONE)
        eng_word_visible.set(View.VISIBLE)
        spn_word_visible.set(View.VISIBLE)
        right_btn_visible.set(View.VISIBLE)
        wrong_btn_visible.set(View.VISIBLE)
        choose_word()
    }

    /**
     * hide status view
     * show all views
     */

    fun hide_main_view() {
        status_layout_visible.set(View.VISIBLE)
        eng_word_visible.set(View.GONE)
        spn_word_visible.set(View.GONE)
        right_btn_visible.set(View.GONE)
        wrong_btn_visible.set(View.GONE)
    }

    /**
     * end of the game
     * show results
     */

    fun on_finish(context: Context) {
        hide_main_view()
        if (wrong_val > right_val) {
            status_txt.set(context.getString(R.string.lose_txt))
        } else {
            status_txt.set(context.getString(R.string.win_txt))
        }
    }


    /**
     * read data from db
     * if nothing exists
     * read data from json file (in assets)
     * and write it to db
     * return the list
     */
    @SuppressLint("CheckResult")
    fun loadData(context: Context) {
        Observable.fromCallable { wordDao.getAll() }.concatMap { db_word_List ->
            if (db_word_List.isEmpty()) {
                Observable.fromCallable { populate_data(context) }.concatMap { insert_list ->
                    wordDao.insertAll(insert_list)
                    Observable.just(insert_list)
                }
            } else {
                Observable.just(db_word_List)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { result ->
                word_list = result
                choose_word()
            }

    }

    /**
     * read data from json file in assets
     * @return string of data in assets
     */

    fun load_json_from_file(context: Context): String {
        try {
            val input_stream: InputStream = context.assets.open("words_v2.json")
            val input_string = input_stream.bufferedReader().use { it.readText() }
            Log.d(TAG, input_string)
            return input_string
        } catch (e: Exception) {
            Log.d(TAG, e.toString())
        }
        return ""
    }

    /**
     * convert string into list of Word
     * @property input_string name of the string
     * @return list of Word
     */

    fun generate_streaming_array(input_string: String): ArrayList<Word> {
        val klaxon = Klaxon()
        val result = arrayListOf<Word>()
        JsonReader(StringReader(input_string)).use { reader ->
            reader.beginArray {
                while (reader.hasNext()) {
                    val word = klaxon.parse<Word>(reader)
                    result.add(word!!)
                }
            }
        }
        return result
    }

    /**
     * Read data from assets file
     * @return a list of Word
     */
    fun populate_data(context: Context): ArrayList<Word> {
        val input_string = load_json_from_file(context)
        return generate_streaming_array(input_string)

    }
}