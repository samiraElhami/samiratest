package com.babbel.samiratest.ui.wordActivity

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import com.babbel.samiratest.BR
import com.babbel.samiratest.R
import com.babbel.samiratest.databinding.ActivityWordBinding
import com.babbel.samiratest.di.ViewModelFactory
import com.babbel.samiratest.persistence.WordDao
import com.babbel.samiratest.util.getScreenHeight
import com.babbel.samiratest.util.random_num
import kotlinx.android.synthetic.main.activity_word.*
import javax.inject.Inject

class WordActivity : AppCompatActivity() {

    @Inject
    lateinit var wordActivityViewModel: WordActivityViewModel

    @Inject
    lateinit var wordDao: WordDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDataBinding()
        wordActivityViewModel.loadData(applicationContext)
        fall_down_word()
    }

    /**
     * binding viewmodel to activity
     */
    private fun performDataBinding() {
        val wordBinding: ActivityWordBinding = DataBindingUtil.setContentView(this, R.layout.activity_word)
        wordActivityViewModel = ViewModelProviders.of(this, ViewModelFactory(this))
            .get(WordActivityViewModel::class.java)
        wordBinding.setVariable(BR.words, wordActivityViewModel)
    }

    /**
     * fall down word
     * generate random number between 10 to 25
     * animate fall down words
     */
    fun fall_down_word() {
        val rand = random_num(10, 25)
        fall_down_word_anim(rand)
    }

    /**
     * Add fall down animation to word
     */

    fun fall_down_word_anim(rand: Int) {
        var count = rand

        var animation = TranslateAnimation(
            0f, // fromXDelta
            0f, // toXDelta
            0f, // fromYDelta
            getScreenHeight()
        )                // toYD
        animation.duration = 10000
        animation.fillAfter = false
        animation.repeatCount = Animation.INFINITE

        spn_word_txt.startAnimation(animation)

        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
                wordActivityViewModel.no_answer(applicationContext)
                count--
                val change_background_runnable = Runnable {
                    wordActivityViewModel.show_main_view()
                    if (count <= 0) {
                        animation?.cancel()
                        wordActivityViewModel.on_finish(applicationContext)
                    }

                }
                Handler().postDelayed(change_background_runnable, 1000)


            }

            override fun onAnimationEnd(animation: Animation?) {
                count--
                val change_background_runnable = Runnable {
                    wordActivityViewModel.show_main_view()
                    if (count <= 0) {
                        animation?.cancel()
                        wordActivityViewModel.on_finish(applicationContext)
                    }

                }
                Handler().postDelayed(change_background_runnable, 1000)

            }

            override fun onAnimationStart(animation: Animation?) {

            }
        })

        on_btn_click(animation)
    }

    fun on_btn_click(animation: Animation) {
        wrong_btn.setOnClickListener {
            animation.cancel()
            animation.start()
            wordActivityViewModel.wrong_answer(applicationContext)
        }

        right_btn.setOnClickListener {
            animation.cancel()
            animation.start()
            wordActivityViewModel.right_answer(applicationContext)
        }
    }

}

