package com.babbel.samiratest.ui.wordActivity.di

import com.babbel.samiratest.persistence.WordDao
import com.babbel.samiratest.ui.wordActivity.WordActivityViewModel

import dagger.Module
import dagger.Provides

@Module
class WordActivityModule {
    @Provides
    fun provideViewModel(wordDao: WordDao) = WordActivityViewModel(wordDao)
}