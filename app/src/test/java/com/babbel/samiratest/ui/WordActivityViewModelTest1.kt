package com.babbel.samiratest.ui

import com.babbel.samiratest.persistence.Word
import com.babbel.samiratest.persistence.WordDao
import com.babbel.samiratest.ui.wordActivity.WordActivityViewModel
import com.babbel.samiratest.util.Status
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.mockkObject
import org.junit.Assert
import org.junit.Before
import org.junit.Test

object Mock_answer_obj {
    fun set_status(status: Boolean) = status
}

class WordActivityViewModelTest1 {
    private lateinit var wordDao: WordDao

    private lateinit var wordActivityViewModel: WordActivityViewModel

    @MockK
    val word_list = listOf(
        Word("holidays","vacaciones"),
        Word("class","curso"),
        Word("bell","timbre"))



    @MockK
    val random = 1

    @Before
    fun setUp() {
        wordDao = mockk(relaxed = true)
        wordActivityViewModel = WordActivityViewModel(wordDao)
    }

    /**
     *  test choose word
      */

    @Test
    fun choose_word_test() {

        Assert.assertEquals(word_list.size, 3)
        Assert.assertEquals(word_list[random].text_eng, "class")
    }

    /**
     * test choose answer
     */
    @Test
    fun choose_answer_test() {

        Assert.assertEquals(word_list[random].text_spa, "curso")
        Assert.assertEquals(false, Mock_answer_obj.set_status(false))
        Assert.assertEquals(true, Mock_answer_obj.set_status(true))

    }

    /**
     * test status
     */
    @Test
    fun status_test() {
        mockkObject(Status.WRONG)
        every { Status.WRONG.status } returns false
        Assert.assertEquals(false, Status.WRONG.status)
    }
}